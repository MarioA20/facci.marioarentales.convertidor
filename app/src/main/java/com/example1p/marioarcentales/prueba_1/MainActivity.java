package com.example1p.marioarcentales.prueba_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButtonFC;
    RadioButton radioButtonCF;
    EditText editTextMensaje;
    Button buttonMensaje;
    TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonFC = (RadioButton) findViewById(R.id.radioButtonFC);
        radioButtonCF = (RadioButton) findViewById(R.id.radioButtonCF);
        editTextMensaje = (EditText) findViewById(R.id.editTextMensaje);
        buttonMensaje = (Button) findViewById(R.id.buttonMensaje);
        textViewResultado = (TextView) findViewById(R.id.textViewResultado);

        buttonMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButtonFC.isChecked()){
                    float valor = Float.parseFloat(editTextMensaje.getText().toString());
                    float resultadoC = (float) ((valor - 32)/(1.8));
                    textViewResultado.setText(String.valueOf(resultadoC));
                }else{
                    float valor = Float.parseFloat(editTextMensaje.getText().toString());
                    float resultadoF = (float) ((valor * 1.8) + 32);
                    textViewResultado.setText(String.valueOf(resultadoF));
                }
            }
        });

    }
}
